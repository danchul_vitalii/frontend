function Orders() {
    this.editOrderData = function (btn) {
        $(btn).removeClass('btn-warning').addClass('btn-success').children('i').remove();
        $(btn).wrapInner('<i class="fas fa-check"></i>');
        $('.btn-warning').attr('disabled', 'true');
        $('.btn-danger').attr('disabled', 'true');
        let items = $(btn).closest('.orders-table-row').find('.user-info')
        items.each(function () {
            $(this).wrapInner(`<textarea class='form-control' ></textarea>`);
        })
        $('textarea').bind('input propertychange', function() {
            $(this).html(this.value);
          });

    }
    this.deleteOrder = function (btn) {
        $(btn).closest('.orders-table-row').remove();
    }

    this.saveOrder = function (btn) {
        $(btn).removeClass('btn-success').addClass('btn-warning').children('i').remove();
        $(btn).wrapInner('<i class="fas fa-edit"></i>');
        $('.btn-warning').removeAttr('disabled');
        $('.btn-danger').removeAttr('disabled');
        let items = $(btn).closest('.orders-table-row').find('.user-info textarea');
        
        items.each(function () {
            console.log('this.contents:', $(this).contents().text());
            $(this).contents().unwrap();
        })

        //Here needs ajax post query to backend.
    }

    this.changePriceByQuantity = function (input) {
        if($(input).hasClass('quantity')) {
            let product = $(input).closest('.orders-table-row').find('.price');
            let productPrice = product.attr('price');
            let discountInput = $(input).closest('.orders-table-row').find('.discount');
            console.log('discountInput.value: ', discountInput.val());
            if(discountInput.val() > 0) {
                let discount = discountInput.val() / 100;
                if(input.value > 1) {
                    let priceWithDiscount = productPrice * input.value - (productPrice * input.value * discount);
                    console.log('priceWithDiscount: ', priceWithDiscount);
                    product.attr('value',priceWithDiscount);
                }
            } else {
                product.attr('value',productPrice * input.value);
            }
        } else if($(input).hasClass('discount')) {
            let product = $(input).closest('.orders-table-row').find('.price');
            let productPrice = product.attr('price');
            let quantityInput = $(input).closest('.orders-table-row').find('.quantity');
            if(quantityInput.val() >= 1) {
                let discountInput = $(input).closest('.orders-table-row').find('.discount');
                let discount = discountInput.val() / 100;
                if(discountInput.val() > 0) {
                    let priceWithDiscount = productPrice * quantityInput.val() - (productPrice * quantityInput.val()* discount);
                    product.attr('value',priceWithDiscount)
                }
            }
        }   
    }
    this.showModal = function (button) {
        $('#exampleModalCenter').append(
            '<div class="modal-dialog modal-dialog-centered" role="document">' +
            '<div class="modal-content">'+
              '<div class="modal-header">'+
                '<h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>'+
                '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
                  '<span aria-hidden="true">&times;</span>'+
                '</button>'+
              '</div>'+
              '<div class="modal-body">'+
                '<div class="input-group mb-3">'+
                   ' <div class="input-group-prepend">'+
                        '<label class="input-group-text" for="inputGroupSelect01">Выберите товар</label>'+
                   ' </div>'+
                    '<select class="selectpicker " data-live-search="true" id="inputGroupSelect01">'+
                        `<option value="..." selected>Имя товара</option>`+
                   ' </select>'+
                '</div>'+
              '</div>'+
              '<div class="modal-footer">'+
                '<button type="button" class="btn btn-primary add-order-to-table" data-dismiss="modal">Save changes</button>'+
              '</div>'+
            '</div>'+
          '</div>'
        );
        productsIntoSelect();
        $('select').selectpicker();
        $('#exampleModalCenter').modal('show')
    }

    this.addOrder = function (button) {
        let productId = $(button).closest('.modal-content').find('option:selected').attr('product-id');
        for(let i = 0; i < db.length; i++) {
            if(db[i].productId === productId) {
                $('.orders-table').append(
                    '<tr class="orders-table-row">' +
                    `<td class='u-data'>${db[i].name}</td>` +
                    `<td class='u-data'><input class="form-control price" type="number" price="${db[i].price}" value="${db[i].price}" disabled/></td>` +
                    `<td class='u-data'><input class="form-control quantity" type="number" min="0" value="1"></td>` +
                    `<td class='u-data'><input class="form-control discount" type="number" min="0"  max="100" value="0"></td>` +
                    `<td class='u-data user-info'>Данные покупателя</td>` +
                    `<td>` +
                    `<button class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                        <button class="btn btn-warning"><i class="fas fa-edit"></i></button>` +
                    `</td>` +
                    '</tr>');
            }
        }
    }
}
