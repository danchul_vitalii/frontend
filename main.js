let Order = new Orders();
let db = [
    {
        "productId": "1",
        "name": "Iphone 8",
        "manufacturer": "Apple",
        "price": "800",
        "currency": "USD",
        "userInfo": "John Michaels, Kiev, +9135003213"
    },
    {
        "productId": "2",
        "name": "Iphone X",
        "manufacturer": "Apple",
        "price": "1000",
        "currency": "USD",
        "userInfo": "John Test, Toronto, +823839302"
    },
    {
        "productId": "3",
        "name": "Iphone XS 512GB",
        "manufacturer": "Apple",
        "price": "1499",
        "currency": "USD",
        "userInfo": "Julie Frost, Madrid, +12345678"
    }
]
let getItems = function () {
    db.map((key, index) => {
        $('.orders-table').append(
            '<tr class="orders-table-row">' +
            `<td class='u-data'>${key.name}</td>` +
            `<td class='u-data'><input class="form-control price" type="number" price="${key.price}" value="${key.price}" disabled/></td>` +
            `<td class='u-data'><input class="form-control quantity" type="number" min="0" value="1"></td>` +
            `<td class='u-data'><input class="form-control discount" type="number" min="0"  max="100" value="0"></td>` +
            `<td class='u-data user-info'>${key.userInfo}</td>` +
            `<td>` +
            `<button class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                <button class="btn btn-warning"><i class="fas fa-edit"></i></button>` +
            `</td>` +
            '</tr>');
    })
};

let productsIntoSelect = function() {
    db.map((key, index) => {
        $('.selectpicker').append(
            `<option value="${key.name}" data-tokens="${key.name}" product-id="${key.productId}">${key.name}</option>`
        )
    })
}

$(document).ready(function () {
    getItems();
    $('table').on('click', '.btn-danger', function () {
        Order.deleteOrder(this);
    })
    $('table').on('click', '.btn-warning', function () {
        Order.editOrderData(this);
    });

    $('table').on('click', '.btn-success', function () {
        Order.saveOrder(this);
    })
    $('body').on('keyup mouseup', '.quantity',  function () {
        this.setAttribute('value', this.value);
        if(this.value > 0) {
            Order.changePriceByQuantity(this);
        } else {
            console.log('wrong value');
        }
        
    });
    $('body').on('keyup', '.search', function() {
        let value = $(this).val().toLowerCase();
        $('body .orders-table tr').filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        }) 
    })
    $('body').on('keyup mouseup', '.discount', function () {
        this.setAttribute('value', this.value);
        if(this.value > 0 ) {
            Order.changePriceByQuantity(this);
        } else {
            console.log('wrong value');
        }
    });

    $('body').on('click', '.add-order',  function () {
        Order.showModal(this);
    })

    $('body').on('change', '.custom-select', function () {;
        this.setAttribute('value', this.value);
        $(this).attr('selected', 'selected');
    });

    $('#exampleModalCenter').on('hidden.bs.modal', function (e) {
        $('option').remove();
    })

    $('body').on('click', '.add-order-to-table', function () {
        Order.addOrder(this);
        
    })
})
